﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using 第四章第四节_重制.Models;

namespace 第四章第四节_重制.Controllers
{
    public class DefaultController : Controller
    {
        private SchoolEntities _shool;
        public DefaultController()
        {
            _shool = new SchoolEntities();
        }

        // GET: Default
        public ActionResult Index()
        {
            ViewBag.pwd = "1";
            ViewBag.name = "1";

            return View();
        }

        [HttpPost]
        public ActionResult Index(string name, string pwd)
        {
            var usertable = _shool.Users.FirstOrDefault(x => x.Usemame == name);

            //判断是否有用户
            if (usertable != null)
            {
                //判断密码是否正确
                ViewBag.name = name;

                if (usertable.Password == pwd)
                {
                    ViewBag.pwd = pwd;

                    //登录OK
                    //获取用户信息
                    Session["user"] = usertable;
                    //去主页
                    return RedirectToAction("gape", "Default");
                }
                else
                {
                    return View();
                }
            }
            else
            {
                return View();
            }


        }


        public ActionResult gape()
        {
            var usertable = _shool.Users.FirstOrDefault(x => x.Usemame == "食堂泼辣酱");
            Session["user"] = usertable;


            var table = _shool.Users.ToList();
                ViewBag.table = table;
                return View();
            //if (Session["user"] != null)
            //{
            //}
            //else
            //{
            //    return RedirectToAction("Index", "Default");
            //}

        }
        [HttpPost]
        public ActionResult gape(int? id)
        {
            var table = _shool.Users.ToList();
            ViewBag.table = table;
            //if (Session["user"] != null)
            //{
            //}
            //else
            //{
            //    return RedirectToAction("Index", "Default");
            //}

            return View();
        }

        public ActionResult Update(int id)
        {
            var table = _shool.Users.FirstOrDefault(x=>x.Userld==id);
            ViewBag.table = table;
            return View();
        }
        [HttpPost]
        public ActionResult Update(User user)
        {
            _shool.Entry(user).State = System.Data.Entity.EntityState.Modified;
            int i =_shool.SaveChanges();
            if (i > 0)
            {
                return RedirectToAction("gape");

            }
            else
            {
                return View();
            }
        }
    }
}